﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {
    public float speed = 0.1f;
    public float jumpDelay = 0.5f;
    private float timestamp;
    public Vector2 jumpH;
    public CircleCollider2D collider;
    private Trigger GroundTrigger;
    public GameObject player;
    //public CircleCollider2D HandsCol;
    //private Trigger ObjectTrigger;

    void Start()
    {
        // Haalt de Trigger script op en assigned de Trigger op de collider
        GroundTrigger = collider.GetComponent<Trigger>();
        //ObjectTrigger = HandsCol.GetComponent<Trigger>();
        player = GameObject.FindGameObjectWithTag("Player");
    }


    void Update()
    {
        // Zorgt ervoor dat de speler niet meer dan één keer kan springen binnen een bepaalde tijd
        if (Time.time >= timestamp && Input.GetKey(KeyCode.Space) && GroundTrigger.isTriggered == true) {
            GetComponent<Rigidbody2D>().AddForce(jumpH, ForceMode2D.Impulse);
            Debug.Log("Jumping");
            timestamp = Time.time + jumpDelay;
        }

        if (Input.GetKeyDown(KeyCode.Z) && player.transform.position.x < 50 && GroundTrigger.isTriggered == true)
        {
            player.transform.position = new Vector2(player.transform.position.x + 60, player.transform.position.y + 1);

        }else if (Input.GetKeyDown(KeyCode.Z) && player.transform.position.x >50 && GroundTrigger.isTriggered == true)
        {
           player.transform.position = new Vector2(player.transform.position.x - 60, player.transform.position.y + 1);
        }

        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            var rb = GetComponent<Rigidbody2D>();
            rb.position += Vector2.left * speed;
        }

        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            var rb = GetComponent<Rigidbody2D>();
            rb.position += Vector2.right * speed;
        }

        if (Input.GetKey(KeyCode.R))
        {
            Application.LoadLevel("Scene 2d");
        }
    }

}
