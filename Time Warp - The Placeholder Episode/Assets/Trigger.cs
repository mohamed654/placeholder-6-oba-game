﻿using UnityEngine;
using System.Collections;

public class Trigger : MonoBehaviour {
    public bool isTriggered;


	void Start () {
	
	}
	

	void Update () {
     
	}

    //Zet isTriggered op true zodra het de grond raakt.
    void OnTriggerEnter2D(Collider2D col)
    {
        isTriggered = true;
        Debug.Log("Touching the ground");
    }


    void OnTriggerStay2D(Collider2D col)
    {
        isTriggered = true;
        //Debug.Log("Is staying on the ground");
    }

    //Zet isTriggered op false zodra het de grond niet raakt.
    void OnTriggerExit2D(Collider2D col)
    {
        isTriggered = false;
        Debug.Log("Has left the ground");
    }



}
