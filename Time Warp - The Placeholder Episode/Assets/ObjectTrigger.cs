﻿using UnityEngine;
using System.Collections;

public class ObjectTrigger : MonoBehaviour {
    public bool isBeingTouched;
    public GameObject movObject;
    //public GameObject[] movObject;
    public GameObject player;
    public BoxCollider2D HandsCol;
    private HandTriggers HTrigger;

    // Use this for initialization
    void Start () {
       //movObject = GameObject.FindGameObjectsWithTag("MovObject");
       movObject = GameObject.FindGameObjectWithTag("MovObject");
       player = GameObject.FindGameObjectWithTag("Player");
       HTrigger = HandsCol.GetComponent<HandTriggers>();
    }
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(KeyCode.Z) && isBeingTouched == true && movObject.transform.position.x < 50 && HTrigger.isTouchingMovObject == true)
        {
            movObject.transform.position = new Vector2(movObject.transform.position.x + 60, movObject.transform.position.y + 1);
            Debug.Log("Object is being warped");
            isBeingTouched = false;
        } else if (Input.GetKeyDown(KeyCode.Z) && isBeingTouched == true && movObject.transform.position.x > 50 && HTrigger.isTouchingMovObject == true)
        {
            movObject.transform.position = new Vector2(movObject.transform.position.x - 60, movObject.transform.position.y + 1);
            Debug.Log("Object is being warped back");
            isBeingTouched = false;
        }

	}

    //Zet isTouching op true zodra een MovObject wordt aangeraak.t
    void OnTriggerEnter2D(Collider2D col)
    {
        if/*(col.gameObject.name == "Player")*/ (player == true)
        {
            isBeingTouched = true;
            Debug.Log("movObject is being touched by a player");
        }
    }

    //Zet isTouching op true zodra een MovObject wordt aangeraakt.
    void OnTriggerExit2D(Collider2D col)
    {
        //if (movObject == null)
        isBeingTouched = false;
        Debug.Log("movObject is not being touched");
    }
}
