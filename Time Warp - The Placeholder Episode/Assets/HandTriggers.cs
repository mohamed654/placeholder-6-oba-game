﻿using UnityEngine;
using System.Collections;

public class HandTriggers : MonoBehaviour {
    public bool isTouchingMovObject;
    public GameObject[] movObject;
    public GameObject player;
    public GameObject[] unmovObject;

    void Start ()
    {
        movObject = GameObject.FindGameObjectsWithTag("MovObject");
        unmovObject = GameObject.FindGameObjectsWithTag("UnmovObject");
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "MovObject")
        {
            isTouchingMovObject = true;
            Debug.Log("Is touching movable object");
        } else
        {
            Debug.Log("Is touching unmovable object");
        }
    }


    void OnTriggerStay2D(Collider2D col)
    {
        if (col.tag == "MovObject")
        {
            isTouchingMovObject = true;
        }
    }


    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "MovObject")
        {
            isTouchingMovObject = false;
            Debug.Log("Player is not touching a movable object anymore");
        } else
        {
            Debug.Log("Player is not touching an unmovable object anymore");
        }
    }



}
